// main
import React from 'react';
import { makeStyles } from '@material-ui/core';
import { Skeleton } from '@material-ui/lab';

const CardSkeleton = () => {

  const useStyles = makeStyles({
    card: {
      width: 170,
      minHeight: 322,
      marginBottom: 30
    },
    img: {
      width: '100%',
      height: 255,
      borderRadius: 8,
    },
    date: {
      fontSize: 14
    }
  });
  const classes = useStyles();

  return (
    <div className={classes.card}>
      <Skeleton variant='rec' animation='wave' className={classes.img} />
      <div style={{margin:'6px 0 3px'}}><Skeleton animation='wave' /></div>
      <div className={classes.date}><Skeleton animation='wave' width='50%' /></div>
    </div>
  );
};

export default CardSkeleton;