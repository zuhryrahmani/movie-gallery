// main
import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core';
import { Skeleton } from '@material-ui/lab';

const CastSkeleton = () => {

  const useStyles = makeStyles({
    card: {
      display: 'flex',
      padding: 5
    },
    img: {
      width: 30,
      height: 45,
      borderRadius: 6,
      margin: 'auto 0',
    },
    info: {
      fontSize: 12,
      display: 'flex',
      marginLeft: 10,
      width: 113
    }
  });
  const classes = useStyles();

  return (
    <div className={classes.card}>
      <Skeleton variant='rect' animation='wave' className={classes.img} />
      <div className={classes.info}>
        <div style={{margin:'auto 0'}}>
          {[1,2].map((item,i) => <Skeleton animation='wave' width={i===0 ? '80px' : '50px'} />)}
        </div>
      </div>
    </div>
  );
};

export default CastSkeleton;