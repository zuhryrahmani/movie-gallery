// main
import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core';

const CastCard = ({image, name, char, id}) => {

  const useStyles = makeStyles({
    card: {
      display: 'flex',
      padding: 5,
      cursor: 'pointer',
      '&:hover': {
        backgroundColor: 'rgba(255,255,255,0.2)'
      }
    },
    img: {
      width: 30,
      height: 45,
      borderRadius: 6,
      margin: 'auto 0',
      background: image ? `url(https://image.tmdb.org/t/p/w200${image}) no-repeat` : '#9e9e9e',
      backgroundSize: 'cover',
      backgroundPosition: 'center',
    },
    info: {
      fontSize: 12,
      display: 'flex',
      marginLeft: 10,
      width: 113
    }
  });
  const classes = useStyles();
  const [nameLink, setNameLink] = useState('');

  useEffect(() => {
    const arr = name.split(' ').map(item => item.toLowerCase());
    setNameLink(`-${arr.join('-')}`);
  }, [name]);

  return (
    <a target='_blank' href={`https://www.themoviedb.org/person/${id}${nameLink}`}>
      <div className={classes.card}>
        <div className={classes.img} />
        <div className={classes.info}>
          <div style={{margin:'auto 0'}}>
            <div>{name}</div>
            <div style={{opacity:0.5}}>{char}</div>
          </div>
        </div>
      </div>
    </a>
  );
};

export default CastCard;