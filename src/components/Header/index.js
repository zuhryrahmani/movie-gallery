// main
import React, { useRef } from 'react';
import { useHistory } from 'react-router-dom';
import { makeStyles, Tabs, Tab } from '@material-ui/core';
import { SearchRounded } from '@material-ui/icons';

const Header = ({tabs, search, searchOpen, setTabs, setTabsTemp, setSearch, setSearchTemp, setSearchOpen, onChangeTabs, onChangeSearch}) => {
  const useStyles = makeStyles({
    header: {
      height: 80,
      borderBottom: '1px solid #404040',
      display: 'flex',
      position: 'fixed',
      zIndex: 10,
      width: '100%',
      backgroundImage: 'linear-gradient(rgba(14,18,26,1) 50%, rgba(14,18,26,0.8) 80%, rgba(14,18,26,0.6))'
    },
    title: {
      width: 'fit-content',
      color: '#13C6B2',
      fontSize: 26,
      fontWeight: 900,
      position: 'absolute',
      zIndex: 1,
      left: 40,
      top: '50%',
      transform: 'translateY(-50%)',
      cursor: 'pointer'
    },
    tabs: {
      flex: 'auto',
      '& .MuiTabs-root': {
        height: '80px',
        '& .MuiTabs-scroller': {
          '& .MuiTabs-flexContainer': {
            height: '80px',
            '& .MuiTab-root': {
              textTransform: 'capitalize',
              fontSize: 16,
              '&.MuiTab-textColorPrimary': {
                color: '#f5f5f5',
                '&.Mui-selected': {
                  color: '#13C6B2'
                }
              }
            }
          },
          '& .MuiTabs-indicator': {
            height: 1,
            backgroundImage: 'linear-gradient(to right, #404040 , #13c6b2)'
          }
        }
      }
    },
    search: {
      padding: 5,
      position: 'absolute',
      right: 40,
      top: '50%',
      transform: 'translateY(-50%)',
      backgroundColor: 'rgba(255,255,255,0.2)',
      borderRadius: '20px',
      display: 'flex'
    },
    searchIcon: {
      width: 24,
      height: 24,
      cursor: 'pointer'
    },
    searchInput: {
      margin: searchOpen ? '0 5px' : 0,
      width: searchOpen ? 100 : 0,
      transition: '0.3s',
      '& input': {
        width: searchOpen ? 100 : 0,
        border: 'none',
        outline: 'none',
        backgroundColor: 'transparent',
        color: '#f5f5f5',
      }
    }
  });
  const classes = useStyles();
  const inputRef = useRef(null);
  const searchRef = useRef(null);
  const history = useHistory();

  const handleSearch = e => {
    e.preventDefault();
    setSearchOpen(!searchOpen);
    setTimeout(() => {
      setSearch('');
    }, 300);
    if(!searchOpen) {
      history.push('/');
      setTimeout(() => {
        inputRef.current.focus();
      }, 300);
    };
  };

  const handleTabsChange = (newValue) => {
    onChangeTabs();
    setTabs(newValue);
  };

  const clickTitle = () => {
    history.push('/');
    setSearchTemp(null);
    setTabsTemp(null);
    setTabs(0);
    setSearch('');
    setSearchOpen(false);
  };

  return (
    <div className={classes.header}>
      <div className={classes.title} onClick={clickTitle}>Movie<span style={{color:'#f5f5f5', fontWeight:300}}>Gallery</span></div>
      <div className={classes.tabs}>
        <Tabs
          value={tabs}
          onChange={(event, newValue) => handleTabsChange(newValue)}
          indicatorColor='primary'
          textColor='primary'
          centered
        >
          {['Popular', 'Recent', 'Top Rated'].map(item => <Tab label={item} />)}
        </Tabs>
      </div>
      <div className={classes.search} id='search' ref={searchRef}>
        <div className={classes.searchInput}>
          <input
            type='text'
            value={search}
            onChange={e => {
              onChangeSearch();
              setSearch(e.target.value);
            }}
            ref={inputRef}
          />
        </div>
        <div className={classes.searchIcon} onClick={e => handleSearch(e)}>
          <SearchRounded color='primary' />
        </div>
      </div>
    </div>
  );
};

export default Header;