// main
import React from 'react';
import { makeStyles } from '@material-ui/core';
import { ImageOutlined } from '@material-ui/icons';
import moment from 'moment';

const Card = ({image, title, date, onClick}) => {

  const useStyles = makeStyles({
    card: {
      width: 170,
      minHeight: 322,
      cursor: 'pointer',
      transition: '0.5s',
      marginBottom: 30,
      '&:hover': {
        transform: 'scale(1.1)'
      }
    },
    img: {
      width: '100%',
      height: 255,
      borderRadius: 8,
      objectFit: 'cover',
      objectPosition: 'center'
    },
    noImg: {
      width: '100%',
      height: 255,
      borderRadius: 8,
      backgroundColor: '#9e9e9e',
      display: 'flex'
    },
    date: {
      fontWeight: 300,
      fontSize: 14,
      color: '#9e9e9e'
    }
  });
  const classes = useStyles();

  const formatTitle = (str) => {
    if(str.length > 35) {
      return `${str.slice(0,31)} ...`;
    } else {
      return str;
    };
  };

  return (
    <div className={classes.card} title={title} onClick={onClick}>
      {image ? (
        <img src={`https://image.tmdb.org/t/p/w500${image}`} className={classes.img} />
      ) : (
        <div className={classes.noImg} >
          <ImageOutlined style={{color:'rgba(0,0,0,0.5)', fontSize:60, margin:'auto'}} />
        </div>
      )}
      <p style={{margin:'6px 0 3px'}}>{formatTitle(title)}</p>
      <p className={classes.date}>{moment(date).format('ll')}</p>
    </div>
  );
};

export default Card;