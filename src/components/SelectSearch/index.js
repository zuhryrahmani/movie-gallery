// main
import React from 'react';
import { makeStyles, Select, MenuItem } from '@material-ui/core';
import { KeyboardArrowDownRounded } from '@material-ui/icons';

const SelectSearch = ({value, onChange, options}) => {

  const useStyles = makeStyles({
    select: {
      color: '#f5f5f5',
      '&.MuiInput-underline:hover:not(.Mui-disabled):before': {
        borderBottom: '2px solid #404040',
      },
      '&.MuiInput-underline:before': {
        borderBottom: '1px solid #404040',
      },
      '&.MuiInput-underline:after': {
        transitionDuration: '0.3s'
      },
      '& .MuiSelect-icon': {
        color: '#13C6B2'
      }
    }
  });
  const classes = useStyles();

  return (
    <>
    <Select
      value={value}
      onChange={onChange}
      inputProps={{ 'aria-label': 'Without label' }}
      className={classes.select}
      IconComponent={KeyboardArrowDownRounded}
    >
      {options.map(item => <MenuItem value={item}>{item}</MenuItem>)}
    </Select>
    </>
  );
};

export default SelectSearch;