// main
import React, { useState, useEffect } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { makeStyles, Button } from '@material-ui/core';
import { StarRounded, PlayArrow, Link, KeyboardBackspaceRounded } from '@material-ui/icons';
import { Skeleton } from '@material-ui/lab';
import axios from 'axios';
import moment from 'moment';

// components
import CastCard from '../../components/CastCard';
import CastSkeleton from '../../components/Skeletons/CastSkeleton';

const Detail = ({tabsTemp, setTabsTemp, searchTemp, setSearchTemp, setTabs, setSearchOpen, setSearch}) => {

  const { id } = useParams();
  const [data, setData] = useState(null);
  const [casts, setCasts] = useState([]);
  const [crews, setCrews] = useState([]);
  const [genres, setGenres] = useState([]);
  const [loading, setLoading] = useState(true);

  const useStyles = makeStyles({
    detail: {
      position: 'relative',
      '& .MuiSkeleton-root': {
        backgroundColor: 'rgba(255, 255, 255, 0.11)'
      },
      '& .MuiSkeleton-wave::after': {
        background: 'linear-gradient(90deg, transparent, rgba(255, 255, 255, 0.1), transparent)'
      }
    },
    container: {
      width: 1000,
      margin: '0 auto',
      padding: '80px 0 50px',
      position: 'relative',
      zIndex: 1
    },
    backdrop: {
      width: '100%',
      height: '100vh',
      position: 'fixed',
      background: `url(https://image.tmdb.org/t/p/original${data?.backdrop_path}) no-repeat`,
      backgroundSize: 'cover',
      backgroundPosition: 'right'
    },
    backdropColor: {
      height: '100%',
      width: '70%',
      backgroundImage: 'linear-gradient(to right, rgba(14,18,26,1) 35%, rgba(14,18,26,0))'
    },
    info: {
      marginTop: 20,
      fontSize: 12
    },
    title: {
      marginTop: 5,
      fontSize: 40,
      fontWeight: 700
    },
    star: {
      marginTop: 15,
      height: 24,
      '& span': {
        verticalAlign: 'top',
        lineHeight: '24px',
        fontWeight: 700,
        fontSize: 20,
        marginLeft: 5
      }
    },
    tag: {
      padding: '8px 17px',
      borderRadius: 20,
      backgroundColor: '#3d3d3d',
      fontSize: 14,
      color: '#f5f5f5',
      fontWeight: 300,
      boxSizing: 'border-box',
    },
    overview: {
      marginTop: 15,
      fontSize: 14
    },
    button: {
      borderRadius: 0,
      padding: 10
    },
    buttonPlay: {
      backgroundColor: '#056358',
      '&:hover': {
        backgroundColor: '#078d7d'
      }
    },
    cast: {
      marginTop: 10,
      display: 'flex',
      flexWrap: 'wrap',
    },
    back: {
      marginTop: 40,
      width: 'fit-content',
      cursor: 'pointer'
    }
  });
  const classes = useStyles();
  const history = useHistory();

  const handleBack = () => {
    history.push('/');
    if(tabsTemp !== null) {
      setTabs(tabsTemp);
      setSearchOpen(false);
      setSearch('');
    };
    if(searchTemp) {
      setSearch(searchTemp);
      setSearchOpen(true);
      // setTimeout(() => {
      //   setSearch(searchTemp);
      // }, 300);
      setTabs(null);
    };
    if(tabsTemp===null && !searchTemp) {
      setTabs(0);
    };
    setTabsTemp(null);
    setSearchTemp(null);
  };

  useEffect(() => {
    axios.get(`https://api.themoviedb.org/3/movie/${id}?api_key=8bc3c74e428d4499f4983ccc8e6bc1ac&language=en-US`)
      .then(res => {
        setData(res.data);
        setGenres(res.data.genres.map(item => item.name));
        setLoading(false);
      })
      .catch(err =>{
        alert(err);
        setLoading(false);
      });
      axios.get(`https://api.themoviedb.org/3/movie/${id}/credits?api_key=8bc3c74e428d4499f4983ccc8e6bc1ac&language=en-US`)
      .then(res => {
        setCasts(res.data.cast);
        setCrews(res.data.crew);
        setLoading(false);
      })
      .catch(err => {
        alert(err);
        setLoading(false);
      });
  }, [id]);

  return (
    <div className={classes.detail}>
      <div className={classes.backdrop}>
        <div className={classes.backdropColor}/>
      </div>
      <div className={classes.container}>
        <div style={{width:500}}>
          <div className={classes.back} onClick={handleBack}><KeyboardBackspaceRounded style={{fontSize:34}}/></div>
          <div className={classes.info}>
            {loading ? <Skeleton animation='wave' width='50%' /> : (
              <div>
                {moment(data?.release_date).format('ll')}
                <span style={{margin:'0 10px'}}>|</span>
                {data?.runtime} minutes
                <span style={{margin:'0 10px'}}>|</span>
                {data?.production_companies.map((item,i) => `${i!==0 ? ', ' : ''}${item.name}`)}
              </div>
            )}  
          </div>
          <div className={classes.title}>
            {loading ? (
              <div>
                <Skeleton animation='wave' />
                <Skeleton animation='wave' width='75%' />
              </div>
            ) : data?.title}
          </div>
          <div className={classes.star}>
            {loading ? (
              <div>
                <Skeleton variant='circle' width='18px' height='18px' animation='wave' style={{display:'inline-block', verticalAlign:'middle'}} />
                <Skeleton animation='wave' width='91.15px' style={{display:'inline-block', marginLeft:11}} />
                <Skeleton animation='wave' width='91.15px' style={{display:'inline-block', marginLeft:30}} />
              </div>
            ) : (
              <div>
                <StarRounded style={{color:'#F9B706'}} />
                <span>{data?.vote_average.toFixed(1)}</span>
                <span style={{fontSize:10, fontWeight:300}}>TMDB Rating</span>
                <span style={{marginLeft:30}}>{data?.vote_count}</span>
                <span style={{fontSize:10, fontWeight:300}}>Voters</span>
              </div>
            )}
          </div>
          {loading ? (
            <div>
              <div style={{margin:'8px 0 15px'}}>
                {[1,2,3].map((item,i) => <Skeleton variant='rect' width='82px' height='33px' animation='wave' style={{borderRadius:20, marginLeft:i===0 ? 0 : 7, display:'inline-block'}} />)}
              </div>
              <div>
                {[1,2].map(item => <Skeleton variant='rect' width='64px' height='46px' animation='wave' style={{display:'inline-block'}}/>)}
              </div>
            </div>
          ) : (
            <div>
              <div style={{margin:'15px 0 25px'}}>
                {genres.map((item,i) => <span className={classes.tag} style={{marginLeft: i===0 ? 0 : 7}}>{item}</span>)}
              </div>
              <div>
                <a href='https://www.youtube.com/' target='_blank'>
                  <Button
                    color='primary'
                    variant='contained'
                    disableElevation
                    className={`${classes.button} ${classes.buttonPlay}`}
                    title='Trailer'
                  >
                    <PlayArrow style={{fontSize:'26px'}}/>
                  </Button>
                </a>
                <a href={data?.homepage} target='_blank'>
                  <Button
                    color='primary'
                    variant='contained'
                    disableElevation
                    className={classes.button}
                    title='Homepage'
                  >
                    <Link style={{fontSize:'26px'}}/>
                  </Button>
                </a>
              </div>
            </div>
          )}
          <div className={classes.overview}>
            {loading ? (
              <div>
                {[1,2,3,4].map(item => <Skeleton animation='wave' />)}
                <Skeleton animation='wave' width='50%' />
              </div>
            ) : data?.overview}
          </div>
          <div style={{marginTop:20}}>{loading ? <Skeleton animation='wave' width='50px' /> : 'Cast :'}</div>
          <div className={classes.cast}>
            {loading ? [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18].map(item => <CastSkeleton />) : casts.map(item => (
              <CastCard
                image={item.profile_path}
                name={item.name}
                char={item.character}
                id={item.id}
              />
            ))}
          </div>
        </div>

      </div>
    </div>
  );
};

export default Detail;