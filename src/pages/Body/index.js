// main
import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { makeStyles, Grow } from '@material-ui/core';
import { Pagination, Skeleton } from '@material-ui/lab';
import axios from 'axios';
import moment from 'moment';

// components
import Card from '../../components/Card';
import SelectSearch from '../../components/SelectSearch';
import CardSkeleton from '../../components/Skeletons/CardSkeleton';

const Body = ({tabs, searchOpen, search, year, setYear, page, setPage, setTabsTemp, setSearchTemp}) => {

  const useStyles = makeStyles({
    container: {
      width: 1000,
      margin: '0 auto',
      padding: '80px 0 50px',
      '& .MuiSkeleton-root': {
        backgroundColor: 'rgba(255, 255, 255, 0.11)'
      },
      '& .MuiSkeleton-wave::after': {
        background: 'linear-gradient(90deg, transparent, rgba(255, 255, 255, 0.1), transparent)'
      }
    },
    search: {
      marginTop: 60,
      textAlign: 'center',
      minHeight: 43
    },
    filter: {
      minHeight: 32,
      '& span': {
        color: '#13C6B2',
        marginRight: 10
      }
    },
    list: {
      marginTop: 30,
      display: 'flex',
      justifyContent: 'space-between',
      flexWrap: 'wrap'
    },
    pagination: {
      marginTop: 20,
      display: 'flex',
      justifyContent: 'center',
      '& .MuiPagination-root': {
        '& .MuiPagination-ul': {
          '& li .MuiPaginationItem-root': {
            color: '#f5f5f5'
          }
        }
      }
    },
    noData: {
      marginTop: 120,
      fontSize: 40,
      fontWeight: 300,
      textAlign: 'center',
      color: 'rgba(255,255,255,0.2)'
    }
  });
  const classes = useStyles();
  const history = useHistory();
  
  const [data, setData] = useState([]);
  const [totalPages, setTotalPages] = useState(0);
  const [loading, setLoading] = useState(true);

  const years = [];
  for(let i=2021; i>=1874; i--) {
    years.push(i);
  };

  const clickCard = (id) => {
    history.push(`/${id}`);
    if(tabs !== null) {
      setTabsTemp(tabs);
    };
    if(searchOpen) {
      setTabsTemp(null);
      setSearchTemp(search);
    };
  };

  useEffect(() => {
    setLoading(true);
    if(!searchOpen && !search) {
      if(tabs !== null) {
        axios.get(`https://api.themoviedb.org/3/discover/movie?api_key=8bc3c74e428d4499f4983ccc8e6bc1ac&language=en-US&sort_by=${tabs===0 ? 'popularity' : tabs===1 ? 'primary_release_date' : 'vote_average'}.desc&include_adult=false&include_video=false&page=${page}${year!=='All' ? `&primary_release_year=${year}` : ''}${tabs===1 ? `&primary_release_date.lte=${moment().format('YYYY-MM-DD')}`: ''}${tabs===2 ? '&vote_count.gte=500' : ''}&with_watch_monetization_types=flatrate`)
          .then(res => {
            setData(res.data.results);
            setTotalPages(res.data.total_pages);
            setLoading(false);
          })
          .catch(err => {
            alert(err);
            setLoading(false);
          });
      };
    } else {
      if(search) {
        axios.get(`
        https://api.themoviedb.org/3/search/movie?api_key=8bc3c74e428d4499f4983ccc8e6bc1ac&language=en-US&query=${search}&page=${page}&include_adult=false&primary_release_year=${year}`)
          .then(res => {
            setData(res.data.results);
            setTotalPages(res.data.total_pages);
            setLoading(false);
          })
          .catch(err => {
            alert(err);
            setLoading(false);
          });
      };
    };
  }, [page, tabs, year, searchOpen, search]);
  console.log('LOOK LOADING', loading);

  return (
    <div className={classes.container}>
      <div className={classes.search}>
        <Grow in={searchOpen}>
          <div>
            <div style={{fontSize:20}}>Search Result</div>
            <div>for <span style={{color:'#13C6B2'}}>"{search}"</span></div>
          </div>
        </Grow>
      </div>
      {searchOpen && !search || searchOpen && data.length===0 ? <div className={classes.noData}>No Search Result</div> : (
        <div>
          <div className={classes.filter}>
            {loading ? <Skeleton animation='wave' width='150px' /> : !searchOpen && (
              <div>
                <span>Filter by year</span>
                <SelectSearch
                  value={year}
                  onChange={e => {
                    setYear(e.target.value);
                    setPage(1);
                  }}
                  options={['All', ...years]}
                />
              </div>
            )}
          </div>
          <div className={classes.list}>
            {loading ? [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20].map(item => (
              <CardSkeleton />
            )) : data.map(item => (
              <Card
                image={item.poster_path}
                title={item.title}
                date={item.release_date}
                onClick={() => clickCard(item.id)}
              />
            ))}
          </div>
          {totalPages > 1 && !loading && (
            <div className={classes.pagination}>
              <Pagination
                count={totalPages}
                page={page}
                onChange={(event, value) => setPage(value)}
                shape='rounded'
                color='primary'
                siblingCount={2}
              />
            </div>
          )}
        </div>
      )}
    </div>
  );
};

export default Body;