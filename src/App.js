// main
import React, { useState, useEffect } from 'react';
import { Switch, Route, useHistory, useLocation } from 'react-router-dom';
import { ThemeProvider } from '@material-ui/core';
import theme from './assets/theme';

// components
import Header from './components/Header';
import Body from './pages/Body';
import Detail from './pages/Detail';

const App = () => {

  const history = useHistory();
  const location = useLocation();

  const [tabs, setTabs] = useState(0);
  const [tabsTemp, setTabsTemp] = useState(null);
  const [searchOpen, setSearchOpen] = useState(false);
  const [search, setSearch] = useState('');
  const [searchTemp, setSearchTemp] = useState(null);
  const [year, setYear] = useState('All');
  const [page, setPage] = useState(1);

  useEffect(() => {
    if(searchOpen) {
      setTabsTemp(tabs);
      setTabs(null);
    } else {
      if(tabsTemp !== null) {
        setTabs(tabsTemp);
      };
      setYear('All');
      setPage(1);
    };
  }, [searchOpen]);

  useEffect(() => {
    if(location.pathname !== '/') {
      setTabs(null);
      setSearch('');
      setSearchOpen(false);
    };
  }, [location]);

  return (
    <ThemeProvider theme={theme}>
      <Header
        tabs={tabs}
        setTabs={setTabs}
        setTabsTemp={setTabsTemp}
        search={search}
        setSearch={setSearch}
        setSearchTemp={setSearchTemp}
        searchOpen={searchOpen}
        setSearchOpen={setSearchOpen}
        onChangeTabs={() => {
          setTabsTemp(null);
          setSearchTemp(null);
          setSearchOpen(false);
          setSearch('');
          setYear('All');
          setPage(1);
          history.push('/');
        }}
        onChangeSearch={() => {
          setYear('All');
          setPage(1);
        }}
      />
      <Switch>
        <Route exact path='/'>
          <Body
            tabs={tabs}
            setTabs={setTabs}
            searchOpen={searchOpen}
            setSearchOpen={setSearchOpen}
            search={search}
            setSearch={setSearch}
            year={year}
            setYear={setYear}
            page={page}
            setPage={setPage}
            setTabsTemp={setTabsTemp}
            setSearchTemp={setSearchTemp}
          />
        </Route>
        <Route exact path='/:id'>
          <Detail
            tabsTemp={tabsTemp}
            setTabsTemp={setTabsTemp}
            searchTemp={searchTemp}
            setSearchTemp={setSearchTemp}
            setTabs={setTabs}
            setSearchOpen={setSearchOpen}
            setSearch={setSearch}
          />
        </Route>
      </Switch>
    </ThemeProvider>
  );

}

export default App;
