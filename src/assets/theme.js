import { createMuiTheme } from '@material-ui/core';

const theme = createMuiTheme({
  typography: {
    fontFamily: 'Lato, sans-serif',
    fontWeight: 'normal',
  },
  palette: {
      primary: {
          light: '#3addca',
          main: '#13C6B2',
          dark: '#078d7d',
          contrastText: '#f5f5f5'
      },
  },
});

export default theme;